@extends('layouts.app')
@section('title', 'Roles')
@section('content')
	@include('layouts.success')
	<div class="container">
		<div class="row">
			<h1>Roles</h1>
		</div>

		<div class="row">
			<a href="/roles/create">
				<button class="btn btn-primary">Crear rol</button>
			</a>
		</div>

		<div class="row">
			<table class="table table-hover">
			  	<thead>
				    <tr>
				      	<th scope="col">#</th>
				      	<th scope="col">Rol</th>
				      	<th scope="col">Descripción</th>
				      	<th scope="col">Ver</th>
				      	<th scope="col">Editar</th>
				    </tr>
			  	</thead>
				<tbody>
				    @foreach($roles as $rol)
				    <tr>
				    	<td>{{$rol->id}}</td>
				    	<td>{{$rol->name}}</td>
				    	<td>{{$rol->description}}</td>
				    	<td><a href="/roles/{{$rol->id}}">Ver</a></td>
				    	<td><a href="/roles/{{$rol->id}}/edit">Editar</a></td>
				    </tr>
				    @endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection