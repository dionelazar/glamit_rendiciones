@extends('layouts.app')
@section('title', 'Editar rol')
@section('content')
	@include('layouts.error')
	<div class="container">
		<div class="row">
	    	<h3>Editar rol</h3>
	    </div>
	    <div class="row">
	    	<form class="form-group" method="POST" action="/roles/{{$rol->id}}">
	    		{{ csrf_field() }}
	  			{{ method_field('PUT') }}
	  			<div class="form-group">
				    <label for="exampleInputEmail1">Rol</label>
				    <input type="text" class="form-control" id="rol" name="rol" placeholder="Escribir rol" value="{{$rol->name}}">
			  	</div>
			  	<button type="submit" class="btn btn-primary">Editar</button>
			</form>
		</div>
		<div class="row">
			{{ Form::open( ['route'=>['roles.destroy', $rol->id], 'method'=>'delete'] ) }}
				<button type="submit" class="btn btn-danger">Eliminar</button>
			{{ Form::close() }}
		</div>
	</div>
@endsection