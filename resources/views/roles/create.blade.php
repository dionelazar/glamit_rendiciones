@extends('layouts.app')
@section('title', 'Crear rol')
@section('content')
	@include('layouts.error')
	<div class="container">
    	<h3>Crear rol</h3>

    	<form class="form-group" method="POST" action="/roles">
    		{{ csrf_field() }}
  			{{ method_field('POST') }}
  			<div class="form-group">
			    <label for="rol">Rol</label>
			    <input type="text" class="form-control" id="rol" name="rol" placeholder="Escribir rol">
		  	</div>
		  	<div class="form-group">
			    <label for="descripcion">Descripción</label>
			    <input type="text" class="form-control" id="description" name="description" placeholder="Escribir descripción">
			    <small id="emailHelp" class="form-text text-muted">Una descripción del rol</small>
		  	</div>
		  	<button type="submit" class="btn btn-primary">Crear rol</button>
		</form>
	</div>
@endsection