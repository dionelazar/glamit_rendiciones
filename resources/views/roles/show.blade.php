@extends('layouts.app')
@section('title', 'Rol')
@section('content')
		<div class="container">
	    	<h3>{{$rol->name}}</h3>
	    	<p>{{$rol->description}}</p>

	    	<div class="row">
	    		<a href="/roles">
	    			<button class="btn btn-primary">Volver</button>
	    		</a>
	    	</div>
	    </div>
@endsection