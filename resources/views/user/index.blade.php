@extends('layouts.app')
@section('title', 'Crear rol')
@section('content')
	@include('layouts.success')
	<div class="container">
		<div class="row">
			<h1>Usuarios</h1>
		</div>

		<div class="row">
			<a href="/user/create">
				<button class="btn btn-primary">Crear usuario</button>
			</a>
		</div>

		<div class="row">
			<table class="table table-hover">
			  	<thead>
				    <tr>
				      	<th scope="col">Nombre</th>
				      	<th scope="col">Apellido</th>
				      	<th scope="col">Email</th>
				      	<th scope="col">Rol</th>
				      	<th scope="col">Estado</th>
				      	<th scope="col">Ver</th>
				      	<th scope="col">Editar</th>
				    </tr>
			  	</thead>
				<tbody>
				    @foreach($users as $user)
				    <tr>
				    	<td>{{$user->name}}</td>
				    	<td>{{$user->lastname}}</td>
				    	<td>{{$user->email}}</td>
				    	<td>{{$user->role_id}}</td>
				    	<td>{{$user->state}}</td>
				    	<td><a href="/user/{{$user->id}}">Ver</a></td>
				    	<td><a href="/user/{{$user->id}}/edit">Editar</a></td>
				    </tr>
				    @endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection