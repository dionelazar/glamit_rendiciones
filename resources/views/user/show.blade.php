@extends('layouts.app')
@section('title', 'Rol')
@section('content')
	<div class="container">
    	<h3><strong>Usuario: </strong>{{$user->name}} {{$user->lastname}}</h3>
    	<p><strong>Email: </strong>{{$user->email}}</p>
    	<p><strong>Registrado: </strong> {{$user->created_at}}</p>
        <p><strong>Roles: </strong>
            @foreach($user->myRoles as $rol)
                {{$rol->role}}
            @endforeach
        </p>

    	<div class="row">
    		<a href="/user">
    			<button class="btn btn-primary">Volver</button>
    		</a>
    	</div>
    </div>
@endsection