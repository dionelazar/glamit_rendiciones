@extends('layouts.app')
@section('title', 'Crear usuario')
@section('content')
	@include('layouts.error')
	<div class="container">
    	<h3>Crear usuario</h3>

    	<form class="form-group" method="POST" action="/user" enctype="multipart/form-data">
    		{{ csrf_field() }}
  			{{ method_field('POST') }}
  			<div class="form-group">
			    <label for="exampleInputEmail1">Nombre</label>
			    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Escribir nombre">
			    <small id="emailHelp" class="form-text text-muted">Tiene que ser un email válido</small>
		  	</div>
		  	<div class="form-group">
			    <label for="exampleInputEmail1">Apellido</label>
			    <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Escribir apellido">
			    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		  	</div>
		  	<div class="form-group">
			    <label for="exampleInputEmail1">Email</label>
			    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Escribir email">
			    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		  	</div>
		  	<div class="form-group">
			    <label for="exampleInputPassword1">Contraseña</label>
			    <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
		  	</div>
		  	<div class="form-group">
			    <label for="exampleFormControlSelect1">Rol</label>
			    <select class="form-control" id="rol" name="rol">
			    	@foreach($roles as $rol)
			    	<option value="{{$rol->id}}">{{$rol->name}}</option>
			    	@endforeach
			    </select>
			  </div>
		  	<div class="form-check">
			    <input type="checkbox" class="form-check-input" id="estado" name="estado">
			    <label class="form-check-label" for="exampleCheck1">Activo</label>
		  	</div>
		  	<div class="form-group">
		  		<input class="form-control" type="file" name="img">
		  	</div>
		  	<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
@endsection