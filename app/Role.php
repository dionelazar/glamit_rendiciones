<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    public function user()
    {
    	return $this->belongToMany('App\User');
    }

    public function insertUserRole($user, $role_id)
    {
        if(DB::table('role_user')->where( ['user_id' => $user->id] )->exists())
        {
        	DB::update('update role_user set role_id = ? where user_id = ?', [$role_id, $user->id]);
        	return false;
        }else{
        	DB::table('role_user')->insert(
	    		['user_id' => $user->id, 'role_id' => $role_id]
	    	);
        }
    }

    public function getUserRol($user)
    {
    	 $role = DB::select('select * from role_user ru JOIN users u ON u.user_id = ru.user_id where user_id = ?', [$user->id]);
    	 return $role;
    }
}
