<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(Request $request)
    {
        //$request->user()->authorizeRoles('admin');
        //var_dump($request);
        //$request->user();
        //$user = new User();
        //$user->authorizeRoles('admin');
    }

    public function index(Request $request, $msg = null)
    {
        $request->user()->authorizeRoles('admin');
        $user = new User();
        $users = $user->all();
        return view( 'user.index', compact('users', 'msg') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles('admin');
        $rol = new Role();
        $roles = $rol->all();
        return view('user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $this->validar($request, 'insert');

        $user = new User();
        $user->name = $request->input('nombre');
        $user->lastname = $request->input('apellido');
        $user->email = $request->input('email');
        $user->password = $this->passwordHash($request->input('password'));
        $user->state = $request->input('estado') == 'on' ? 1 : 0;
        if($request->hasFile('img'))
        {
            $file = $request->file('img');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
            $user->img = '/images/'.$name;
        }
        $user->save();
        $this->updateRole($user, $request->input('rol'));
        $msg = 'Creado correctamente';
        return redirect()->route('user.index')->with('msg', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $myUser = new User();
        $user = $myUser->find($id);
        $user->getMyRoles();

        return view( 'user.show', compact('user') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->user()->authorizeRoles('admin');
        $user = User::find($id);
        $rol = new Role();
        $roles = $rol->all();
        return view( 'user.edit', compact('user', 'roles') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $this->validar($request, 'update');

        $user = User::find($id);
        $user->name = $request->input('nombre');
        $user->lastname = $request->input('apellido');
        $user->email = $request->input('email');
        if($request->input('password') != '')
        {
            $validateData = $this->validate($request, ['password' => 'required|min: 5']);
            $user->password = $this->passwordHash($request->input('password'));
        }
        $user->state = $request->input('estado') == 'on' ? 1 : 0;
        if($request->hasFile('img'))
        {
            $file = $request->file('img');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
            $user->img = '/images/'.$name;
        }
        $user->save();
        $this->updateRole($user, $request->input('rol'));
        $msg = 'Editado correctamente';
        return redirect()->route('user.index')->with('msg', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();
        $msg = 'Eliminado correctamente';
        return redirect()->route('user.index', [$rol])->with('msg', $msg);
    }

    public function login($request)
    {
        $user = User::where('email', '=', $request->input('email'))->firstOrFail();
        
        if($user)
        {
            if(passwordVerify($request->input('email'), $user->password))
            {
                return redirect()->route('user.index');
            }
            return redirect('/');
            //return 'Contraseña incorrecta';
        }
        return redirect('/');
        //return 'Email incorrecto';
    }

    private function updateRole($user, $role_id)
    {
        $role = new Role();
        $role->insertUserRole($user, $role_id);
    }

    private function passwordHash($pass)
    {
        $hash = bcrypt($pass);
        //$hash = password_hash($pass, PASSWORD_DEFAULT);
        return $hash;
    }

    private function isImage($string)
    {
        $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/bmp','image/svg+xml'];
        $contentType = mime_content_type('path/to/image');

        if(! in_array($contentType, $allowedMimeTypes) ){
          return false;
        }

        return true;
    }

    private function validar($request, $action)
    {

        $insert = [
            'rol' => 'required',
            'nombre' => 'required|max: 100',
            'rol' => 'required',
            'apellido' => 'required|max: 100',
            'email' => 'required|email|unique:users,email',
            'email' => 'required|email',
            'password' => 'required|min: 5',
            //'estado' => 'required',
        ];

        $uptate = [
            'rol' => 'required',
            'nombre' => 'required|max: 100',
            'rol' => 'required',
            'apellido' => 'required|max: 100',
            'email' => 'required|email',
        ];

        $actions = array('insert' => $insert, 'update' => $uptate);
        $validateData = $this->validate($request, $actions[$action]);

        return $validateData;
    }
}
