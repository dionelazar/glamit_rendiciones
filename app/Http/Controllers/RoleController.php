<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rol = new Role();
        $roles = $rol->all();
        return view( 'roles.index', compact('roles') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $this->validar($request);

        $rol = new Role();
        $rol->name = $request->input('rol');
        $rol->description = $request->input('description');
        $rol->save();
        $msg = 'Creado correctamente';
        return redirect()->route('roles.index', [$rol])->with('msg', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rol = Role::find($id);
        return view( 'roles.show', compact('rol') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = Role::find($id);
        return view( 'roles.edit', compact('rol') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $this->validar($request);
        //$rol = new Role();
        $rol = Role::find($id);
        //$rol->find($id);
        $rol->name = $request->input('rol');
        //$rol->fill();
        $rol->save();
        $msg = 'Editado correctamente';
        return redirect()->route('roles.index', [$rol])->with('msg', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rol = Role::find($id);
        $rol->delete();
        $msg = 'Eliminado correctamente';
        return redirect()->route('roles.index', [$rol])->with('msg', $msg);
    }

    private function validar($request)
    {
        $validateData = $this->validate($request, [
            'rol' => 'required|max: 100',
        ]);

        return $validateData;
    }
}
