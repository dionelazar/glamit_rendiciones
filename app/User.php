<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    public $myRoles;

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function authorizeRoles($roles)
    {
        if($this->hasAnyRole($roles))
        {
            return true;
        }
        
        abort(401, 'No estás autorizado');//No autorizado
    }

    public function hasRole($role)
    {
        if($this->roles()->where('name', $role)->first())
        {
            return true;
        }
        
        return false;
    }

    public function hasAnyRole($roles)
    {
        if(is_array($roles))
        {
            foreach ($roles as $role) {
                if($this->hasRole($role))
                    return true;
            }
        }else{
            if($this->hasRole($roles))
                return true;
        }

        return false;
    }

    public function getMyRoles()
    {
        $role = DB::select('SELECT ru.role_id as role_id, r.name as role
                                FROM role_user ru 
                                JOIN users u ON u.id = ru.user_id
                                JOIN roles r ON r.id = ru.role_id
                                WHERE u.id = ?'
                            , [$this->id]);

        $this->myRoles = $role;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'lastname', 'state', 'img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
