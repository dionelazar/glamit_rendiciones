<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'user')->first();

        $user = new User();
        $user->name = "User";
        $user->email = "user@mail.com";
        $user->password = bcrypt('diodio');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = "admin";
        $user->email = "dazar@glamit.com.ar";
        $user->password = bcrypt('diodio');
        $user->save();
        $user->roles()->attach($role_admin);
    }
}
